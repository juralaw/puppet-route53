puppet-route53
==============

This is a module to manage DNS for a server using Amazon Web Services Route53 service.

It has 2 parts:
1. Install the Route53 tools
2. Call the Route53 service to configure DNS

Note:  This is intended to be used when deploying to cloud environments.  Do not use this module when you are running vagrant VMs on localhost.  This module works best when also using the framework-puppet and puppet-hostname repositories.

Usage:
======

With Hiera (json backend)
```
{
    "classes":[
        "route53"
    ],
    "route53::secret_key":"%{ec2_secret_key}",
    "route53::access_key":"%{ec2_access_key}",
    "route53_entry": {
        "host": {
            "zone": "example.com",
            "values": "%{ec2_public_ipv4}"
        }
    }
}
```

Without Hiera:
```
class { 'route53':
    secret_key  => 'MY_AWS_SECRET_KEY',
    access_key   => 'MY_AWS_ACCESS_KEY',
}
class { 'route53::host':
    zone    => 'example.com',
}
```

