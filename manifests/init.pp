# Route 53
# Will update a route 53 configuration
class route53 (
    $secret_key,
    $access_key,
    $ensure     = '0.3.1',
    $zone = 'UNSET'
    ){

    # Copy route53 configuration to root home directory
    file { '/root/.route53':
        mode      => "0600",
        content => "---
default_ttl: \"300\"
secret_key: $secret_key
access_key: $access_key
api: \"2011-05-05\"
endpoint: https://route53.amazonaws.com/",
    }

    # Ensure that route 53 gem is installed
    package { "route53":
        ensure   => $ensure,
        provider => 'gem'
    }

}
