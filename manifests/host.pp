# Route 53
# Will update a route 53 configuration
# The zone parameter should be like example.com (no trailing period)
define route53::host (
    $zone = $route53::zone,
    $hostname = $::fqdn,
    $recordtype = 'A',
    $values = $::ipaddress
    ){

    $route53args = " --zone ${zone}. --name ${hostname}. --type ${recordtype} --values ${values} "

    exec { "${name}_route53_create":
        command     => "route53 -c ${route53args}",
        environment => ['HOME=/root'],
        unless      => "test \"$(route53 -l ${zone}. | grep -c ${hostname})\" -eq \"1\"",
        require     => [ Package['route53'], File['/root/.route53'] ],
        logoutput   => true,
    }

    exec { "${name}_route53_update":
        command     => "route53 -g ${route53args}",
        environment => ['HOME=/root'],
        unless      => "test \"$(route53 -l ${zone}. | grep ${hostname} | grep -c ${values} )\" -eq \"1\"",
        require     => [ Package['route53'], File['/root/.route53'] ],
        logoutput   => true,
    }
}
